package model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "aadress")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    @SequenceGenerator(name = "my_seq", sequenceName = "jarjend", allocationSize = 1)
    private Long id;

    @Column(name = "tanav")
    private String street;

    public Address(String street) {
        this.street = street;
    }
}
